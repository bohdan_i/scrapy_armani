

import sys
import pandas as pd
import numpy as np

if (len(sys.argv) != 2):
	print("Using: process_csv_result.py result.csv")
	exit(1)

filename = sys.argv[1]

df = pd.read_csv(filename)

by_region = df.groupby('region')['name'].count()
by_region = by_region.rename("items amount by region")

check_curr = df.groupby('region')
check_curr_res = '"check_curr": {'
for name, group in check_curr:
	status = 'OK'
	if (1 !=group['currency'].apply(lambda x: (name == 'fr' and x == 'EUR' or name == 'us' and x == '$') and 1 or 0).prod()):
		status = 'fail'
	check_curr_res += "\"%s\": %s, " % (name, status)
check_curr_res += "}"

print ('{"by_region_amount": %s, "check_currency": %s, }' % (by_region.to_json(), check_curr_res) )